---
title: Hành trình về Phương Đông
author: Blair T. Spalding (Nguyên Phong dịch)
rating: 4.5
affiliate: tiki
afflink: https://shorten.asia/76tvWd4P
tags: ["vietnamese"]
weight: 3
---

#### Giới thiệu  
Đầu tiên, theo mình đây là một cuốn sách hết sức thú vị. Tuy nhiên, sách sẽ hơi kén độc giả vì có khuynh hướng tâm linh  
Bỏ qua các yếu tố thần thông hoá trong sách, thì cuốn sách có thể mang tới nhiều giải thích tâm linh theo cách khoa học. 
Đọc sách bạn có thể vỡ lẽ ra vài điều mà trước đây từng thắc mắc, như:  

- Yoga có phải chỉ là một môn thể dục thể thao ?
- Sau khi chết người ta sẽ đi đâu ?
- Có ma không ? Ma là gì ?

Với một chủ đề khó như thế này nhưng bản dịch của giáo sư **Nguyên Phong** phải nói là cực kỳ dễ hiểu với cách chọn từ khá chuẩn xác. 
Thiết nghĩ mình đọc phiên bản tiếng Anh chắc phải mất gấp đôi thời gian

#### Khoa học phương Tây  
Thời đại của chúng ta là thời đại khoa học phát triển rực rỡ, mà các quốc gia phương Tây là những nước dẫn đầu. 
Người phương Tây, do đó, cũng tự tin và tin tưởng tuyệt đối vào khoa học.  

Tuy nhiên, nhìn lại lịch sử, phương Đông cũng từng có thời kỳ như vậy. 

#### Huyền bí phương Đông 