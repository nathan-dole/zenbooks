---
title: Những kẻ xuất chúng
author: Malcolm Gladwell
rating: 5
affiliate: tiki
afflink: https://shorten.asia/MnWyvhde
weight: 2
reading_time: 8 tiếng
tags: ["vietnamese"]
---

#### Giới thiệu
</br>
**Outliers (Những kẻ xuất chúng)** có lẽ là một trong những cuốn sách hay nhất mình từng đọc, và đọc xong cũng trong thời gian khá ngắn **(8~10h)**.  
  
Cuốn sách vén lên ***bức màn bí mật của sự thành công***. 

Và trái với quan điểm của số đông, thường là ca tụng nỗ lực cá nhân, cuốn sách đưa ra một cái nhìn hoàn toàn mới trên góc độ xã hội.   

Bản dịch với văn phong khá mượt mà, liên tục đưa ra những mẩu chuyện với nhiều câu hỏi đặt ra lôi cuốn người đọc như xem một cuốn truyện trinh thám.  

Bằng cách thấy rõ hơn vai trò của **môi trường, xã hội và di sản văn hoá** đối với thành công, người đọc có thể tìm cách lặp lại các điều kiện tương ứng để thành công dễ đến hơn.   
  
{{< img "images/success.jpg" "Success" >}}
</br>

#### Những kẻ xuất chúng  
Hmm ... vậy thì tóm lại là **Bill Gates**, ban nhạc **The Beatles** và các **luật sư** cự phách phố Wall thì có điểm gì chung ?   

Có thật là tất cả họ đều thành công từ 2 bàn tay trắng không ? *Hẳn nhiên rồi*, vì không phải là ai cũng muốn một câu chuyện anh hùng **một mình chống lại** mọi khó khăn sao ?  

Ghi chú: Sách còn nhiều mẩu chuyện hay đưa ra mối liên hệ giữa thành công và một số 
</br>
</br>

##### Quy tắc 10,000 giờ  
Ắt hẳn bạn đã nghe ở đâu đó rằng muốn thực sự thành thạo một kỹ năng gì đó (từ chơi piano đến lập trình), người ta đều phải tốn 10,000 giờ tập luyện. 
Dựa vào công thức này thì nếu bạn tập 5 tiếng mỗi ngày không nghỉ cho 1 năm thì bạn có ~ **1,800 giờ**.  

Điều này cũng có nghĩa là để tập bất cứ thứ gì để thuần thục đến mức chuyên nghiệp bạn phải mất ít nhất 5 năm tập luyện cật lực (5 tiếng mỗi ngày không nghỉ).  

Và điểm mấu chốt của những câu chuyện sau đều nằm ở đây. Trước tiên hãy nhìn vào ban nhạc **The Beatles**  
</br>

##### The Beatles, ban nhạc Anh huyền thoại  

{{% 2cols left-cls="col-sm-7" right-cls="col-sm-5" img-src="images/beatles.jpg" img-alt="The Beatles" %}}{{% blockquote %}}
Beatles -  với bốn thành viên John Lennon, Paul McCartney, George Harrison  và Ringo Starr - đặt chân tới nước Mỹ vào tháng 2 năm 1964, bắt đầu cái gọi là *Cuộc xâm lăng của Anh Quốc vào sân khấu âm nhạc Hoa Kỳ* và cho ra đời một loạt các bản thu âm đỉnh cao đã biến đổi hoàn toàn diện mạo của nền âm nhạc đại chúng
{{% /blockquote %}}{{% /2cols %}}

The Beatles thành lập năm 1957, 7 năm trước khi đặt chân đến Mỹ. Đi ngược thời gian về năm 1960, lúc này The Beatles mới chỉ là một ban nhạc rock của trường trung học, họ được mời đến chơi nhạc ở **HamBurg** (Đức).  

Trước thời điểm này, ở **HamBurg** không hề có các CLB rock-and-roll mà chỉ có CLB vũ thoát y. Có một người chủ CLB tên **Bruno** nảy ra sáng kiến cho các ban nhạc chơi nhạc và thuyết phục các CLB khác làm theo tương tự. Thêm một điểm tình cờ nữa là dù Bruno đến London để tìm ban nhạc nhưng lại gặp được một thương gia từ **Liverpool**, nơi The Beatles được thành lập. Ông này giới thiệu **The Beatles** cho **Bruno**, và từ đó họ được chơi cho các CLB ở HamBurg.  

À, nhưng bạn tự hỏi, chơi ở **HamBurg** thì giúp ích gì cho ban nhạc nào ? Tiền thù lao cao ? Khán giả có gu nghe nhạc ?  
Hoàn toàn không phải. Mà đó là **khối lượng thời gian tuyệt đối ban nhạc buộc phải chơi**. Họ phải chơi 8 tiếng mỗi ngày, 7 ngày một tuần...    

{{%blockquote footer="John Lennon"%}}
Chúng tôi chơi khá hơn và thêm tự tin. Đó là tất yếu do những trải nghiệm chơi nhạc suốt đêm dài mang lại. Thêm cái lợi nữa là họ là người nước ngoài. Chúng tôi phải cố gắng thậm chí nhiều hơn nữa, trút tất cả trái tim và tâm hồn mình vào đó, để vượt qua chính mình 
 
Ở Liverpool, chúng tôi mới chỉ chơi trong những suất diễn dài một tiếng đồng hồ và chúng tôi chỉ chơi đi chơi lại một số ca khúc tủ của mình trong mọi buổi biểu diễn.  
Ở HamBurg, chúng tôi phải chơi suốt 8 tiếng đồng hồ, nên chúng tôi phải thực sự tìm một cách chơi mới
{{% /blockquote %}}
</br>

##### Bill Gates, thần đồng toán học, lập trình và nhà kinh doanh tài ba tạo nên đế chế Microsoft

Nếu nhìn sơ qua thì thành công của Bill Gates chắc hẳn đến từ sự thông minh và tài ba của riêng ông. Nhưng có thật đó là tất cả ?

Cha của Bill Gates là một luật sư giàu có ở Seattle, và vì Bill chán học hành nên cha mẹ ông đã gửi ông vào một trường tư danh giá tên là Lakeside từ năm lớp 7. Và với điều kiện kinh tế dư giả của trường thì năm 1968, hội phụ huynh đã mua một trong những máy tính tiên tiến nhất lúc đó mà ngay cả các trường Đại học cũng không có. **ASR-33 Teletype**.  

Và từ đó đam mê của Bill Gates cũng được nhen nhóm. Ông dành năm năm ngay từ năm lớp 8 đến tận cuối trung học, lập trình và tìm hiểu về máy tính. Đây cũng là tiền đề cho những công việc đầu tiên của ông (do khi đó hiếm có người có điều kiện và khả năng tương tự).


{{% 2cols left-cls="col-sm-7 push-sm-5" right-cls="col-sm-5 pull-sm-7" img-src="images/coding.jpg" img-alt="Bill Gates" %}}{{%blockquote%}}
"Đó là nỗi ám ảnh của tôi", Gates kể về những năm đầu trung học. "Tôi bỏ tiết thể dục, tôi đến đó vào ban đêm. Chúng tôi lập trình vào cuối tuần. Hiếm có tuần nào mà chúng tôi không tiêu tốn vào đó 20, 30 tiếng đồng hồ... Khi ấy tôi mới 15,16 tuổi
{{% /blockquote %}}{{% /2cols %}}
</br>

#### Vén màn những bí mật của thành công
**Nhận xét:** Xuyên suốt cuốn sách này là vô vàn những câu chuyện về thành công mà nhìn bề ngoài có vẻ như hoàn toàn là do nỗ lực cá nhân. Nhưng nhìn sâu bên trong là rất nhiều điều kiện thuận lợi lẫn bất lợi tạo nên sự thành công ấy. 

- Tại sao Korean Airlines trước 1990 để xảy ra nhiều vụ đâm máy bay (câu trả lời của cuốn sách thật sự ấn tượng)
- Tại sao những người có IQ cao thuộc hàng đầu thường lại không thành công vượt bật trong cuộc sống
- Tại sao người Trung Quốc (hay châu Á nói chung) giỏi toán
- Tại sao các trẻ em thuộc gia đình nghèo (ở Mỹ) thường khó học giỏi  

#### Tóm lại   
Một cuốn sách hay với lối kể chuyện lôi cuốn. Cách đặt vấn đề gây tò mò thích thú cho người đọc. 
Dù là đọc cho vui hay để suy ngẫm về những bí ẩn thú vị trong sách thì đều rất hay.