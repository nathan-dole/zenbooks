---
title: Thói quen thành công của những triệu phú tự thân
original_title: "Rich Habits: The Daily Successful Habits of Wealthy Individuals"
author: Thomas C. Corley
affiliate: tiki
afflink: https://shorten.asia/Cscnazwj
rating: 4
weight: 4
tags: ["vietnamese"]
---

#### Giới thiệu
Một cuốn sách rất ngắn có thể đọc trong 4 tiếng. 
Văn phong và cốt truyện dễ hiểu, tuy nhiên phần thực sự hữu dụng và mới mẻ chỉ là 30 thói quen để thành công.
Về mặt hấp dẫn hoặc súc tích, chắc chắn là không thể bằng  [Người giàu có nhất thành Babylon]({{< ref "./Nguoi-giau-co-nhat-thanh-Babylon" >}} "Người giàu có nhất thành Babylon")


Tuy nhiên, với góc nhìn hiện đại hơn, điều cuốn sách có thể mang lại là làm cho người đọc thấy dễ và thực hành được ngay.
Chỉ 2 điểm sau thôi là đã giúp cuốn sách được đánh giá hơn một chút:  

- **Giúp người đọc tự nhìn nhận lại thói quen xấu**, có khả năng huỷ hoại mọi cơ hội thành công  
Những việc bình thường mà ai cũng làm như xem TV, phim ảnh, đọc báo vẩn vơ hay tám chuyện người khác.
Đây đều có thể là thói quen xấu vì nó lấy hết thời gian của bạn đáng ra phải dành cho thói quen tốt
- Khi mọi thứ nó được đưa về hệ quy chiếu là mỗi ngày thì **áp lực thực hiện thói quen cũng nhỏ lại**.  
Bạn không cảm giác phải chạy suốt 30 ngày phía trước. Quan trọng là hoàn thành chỉ tiêu hôm nay.

<br>

#### Cách thói quen hoạt động  
{{% 2cols left-cls="col-sm-7" right-cls="col-sm-5" img-src="images/brain.jpg" img-alt="Habit" %}}
Tóm tắt một cách đơn giản nhất, thói quen là để giúp bộ não tiết kiệm năng lượng, không phải liên tục ra quyết định.
Theo cuốn sách, có 3 phần của bộ não:  

- **Vỏ não (Não mới)**: phần mới hình thành sau này, chúng ta suy nghĩ và có ý thức hầu hết ở vùng này
- **Hệ viền**: Phần lâu đời thứ 2 của não, kiểm soát nhiều chức năng, gồm cả thói quen
- **Thân não**: lâu đời nhất và kiểm soát hoạt động tự động như lưu thông máu, hít thở, tim mạch.  
{{% /2cols %}}

Khi bạn hình thành thói quen thì chúng chuyển vào hệ viền và hoạt động như chế độ tự động trên máy bay.
Vì việc làm khác với thói quen có sẵn gây tốn năng lượng cho não nên thường các thói quen sẽ khó thay đổi một khi đã hình thành.  
Cuốn sách cũng chỉ ra một số tác nhân kích hoạt việc thay 
đổi hay tạo thói quen mới.

<br>

#### 30 thói quen giàu có  
Thực sự thì 30 thói quen có hơi nhiều quá và đôi chỗ lặp lại. Riêng về phần này, chỉ những thói quen quan trọng mới được liệt kê ở đây

{{% 2cols left-cls="col-sm-7 push-sm-5" right-cls="col-sm-5 pull-sm-7" img-src="images/target.jpg" img-alt="Focus" %}}

- **Thói quen 1:** Tôi sẽ tạo thói quen tốt và làm theo các thói quen này mỗi ngày  
-  **Thói quen 2:** Tôi sẽ xác định ước mơ và lập mục tiêu cho từng ước mơ của mình. Rồi tập trung vào các mục tiêu này mội ngày  
-  **Thói quen 3:** Tôi sẽ dành ít nhất 30 phút mỗi ngày để nâng cao kiến thức và cải thiện kỹ năng bản thân. Đầu tư cho bản thân mỗi ngày
-  **Thói quen 4:** Tôi sẽ dành ít nhất 30 phút mỗi ngày để tập thể dục. Tôi sẽ ăn uống lành mạnh mỗi ngày

{{% /2cols %}} 
<br>
{{% 2cols left-cls="col-sm-7" right-cls="col-sm-5" img-src="images/exercise.jpg" img-alt="Healthy" %}}
-  **Thói quen 5:** Tôi sẽ tìm cách xây dựng mối quan hệ bền vững với những người có chí hướng thành công khác
-  **Thói quen 7-15-18:** Thực hiện mục tiêu  
   
    - Tôi sẽ hành động theo mục tiêu mỗi ngày (TQ 7)
    - Tập trung vào mục tiêu mỗi ngày (TQ 15)
    - Thực hiện mục tiêu 1 cách kiên nhẫn (TQ 18)

- **Thói quen 8-16-22:** Tư duy

   - Thực hành tư duy tích cực mỗi ngày  (TQ 8)
   - Chỉ đặt mục tiêu tốt, tránh xa mục tiêu xấu  (TQ 16)
   - Không để sợ hãi hay ngờ vực bản thân ngăn cản hành động (TQ 22)

- **Thói quen 9:** Tiết kiệm và đầu tư ít nhất 10% thu nhập
- **Thói quen 14:** Tôi sẽ tìm đến các cố vấn thành công
{{% /2cols %}}   

#### Tóm lại  
Một cuốn sách đáng mua và thực hành theo. Tuy nhiên đừng hy vọng quá nhiều vào cốt truyện và nhân vật trong đó.  
Cuốn sách gần giống một cuốn sách bài tập hơn là sách lý thuyết.  
