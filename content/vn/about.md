---
title: Về Zenbooks
subtitle: What's this site about ?
comments: false
---

#### Zenbooks là gì ?
Một tập hợp các tóm tắt và review sách mình đọc.  


#### Mục tiêu của Zenbook ?  
{{% divcls cls="wow fadeInUp" %}}  

##### 1. Ghi lại cho nhớ  
Chủ yếu để tóm tắt và review lại sách. Có những điều nên ghi nhớ và sử dụng lâu dài.
{{% /divcls %}}  

{{% divcls cls="wow fadeInUp" %}}  

##### 2. Thói quen đọc sách ở Việt Nam  
Theo một bản khảo sát cách đây không lâu thì người Việt ít chịu đọc sách. Lượng thư viện ở Việt Nam cũng không nhiều (so với Singapore).  

Zenbooks muốn xây dựng thói quen đọc sách ở Việt Nam bằng cách đưa ra nhiều đánh giá, tóm tắt sách để các bạn lựa chọn.
{{% /divcls %}}  

{{% divcls cls="wow fadeInUp" %}}

##### 3. Nghe nhìn v.s. đọc  
Hiện nay thì nghe nhìn (xem clip/video/TV) dường như thay thế văn hoá đọc. Dù cho là clip giáo dục hay chỉ là video nhảm nhí thì video vẫn có ưu thế hơn.    

Xem clip nhìn qua thì có vẻ sẽ tiếp nhận thông tin nhanh hơn và nhiều hơn. Tuy nhiên theo mình thấy khi xem video, thì chủ yếu chúng ta tiếp thu thông tin một cách thụ động hoặc cùng lắm là phản xạ (reactive).  
Nghĩa là không có đủ thời gian để mà suy nghĩ hay nghiền ngẫm về thông tin nhận được. Và vì vậy thường cũng không có gì đọng lại.  

Về mặt này thì đọc lại chiếm ưu thế, bạn luôn luôn phải hiểu thông tin đang nhận được thì mới tiếp tục được. Và theo một nghiên cưú thì đọc là một trong các cách cải thiện khả năng diễn đạt rất hiệu quả.   
{{% /divcls %}}  

##### 4. Trang web đơn giản và nhanh  
Zenbooks muốn người đọc có trải nghiệm tốt nhất nên luôn cố gắng có tốc độ load nhanh nhất và đơn giản nhất có thể.  

Đọc phải nên là một trải nghiệm chứ không nên là một nghĩa vụ.  
Nếu một ngày nào đó mà người Việt thích đọc sách hơn đi uống bia thì có lẽ zenbooks không cần tồn tại nữa.  

##### 5. Tiếp thị liên kết  
Ngoài việc tóm tắt sách, zenbooks luôn có chèn vào link tiếp thị để bạn mua sách.
Đương nhiên đây là cách zenbooks có thu nhập. Tuy nhiên đó cũng là cách bạn ủng hộ tác giả của các quyển sách bằng cách mua và đọc chúng.  

##### 6. Cộng tác
Nếu bạn có hứng thú với việc tóm tắt sách và muốn cộng tác với zenbooks hãy email vào [zenbooks.io.10@gmail.com](mailto:zenbooks.io.10@gmail.com)
