---
title: Minh Mạng - Vị Hoàng Đế sinh nhầm thế kỷ
author: Hùng Ca Sử Việt
tags: ["vietnamese"]
disable: true
---
Như có nói trước đây, vấn đề của Sử Việt trong trường học là tư tưởng dân tộc chúng ta luôn là kẻ bị hại. 
Hầu hết chúng ta không biết Việt Nam từng mạnh như thế nào. 
Điều này dẫn tới tư duy nhược tiểu và thụ động (“bất khuất” chứ ko phải “hùng mạnh”).   

Mình chia sẻ bài này để thay đổi tư duy đó. Sau đây là bài viết về Minh Mạng của kênh **Hùng Ca Sử Việt**, do **Đạt Phi** đọc.


<div class="text-center">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/2adwvUSXuf4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>