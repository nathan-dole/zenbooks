---
title: Start with Why
author: Simon Sinek
rating: 4.5
tags: ["english"]
affiliate: amazon
afflink: https://amzn.to/4a12tkM
---

#### Introduction:
Though to me, it was not a quick or easy book to read. Because it requires some **thinking** and **reflecting**.  

The things that it has pointed out to me, and hopefully to you too, are very important. For some of us, our whole lives have sort of happened by accident. What we've decided to choose as our career, who are our friends, the major parts of that have not been decided by us consciously.  

{{< img "images/tired.jpg" "Tired" >}}  

We ain't sure why we are doing this. Our guts might tell us *this feels right*, but we cannot really put it down to words. Why am I going to work every day ? Why am I doing what I am doing now, having a relationship with my current partner ? I am not quite sure how many percent of us has been pondering this question. But my wild guess is it is the majority of us. How many of you have been changing from job to job without finding that job satisfaction that you've always dreamt of. That is exactly the signal of an unclear **WHY**.  

I was experiencing a fuzzy **WHY** when starting reading the book. Therefore, it has taken me pretty long to process the idea and reflect on it. 
<hr/>   

#### Why should we start with WHY ?  
As mentioned above, I think we are living in a world where most people don't know what they are aiming for. It is ***a world that doesn't start with WHY***.   

##### The focus on WHATs

{{% 2cols left-cls="col-sm-8" right-cls="col-sm-4" img-src="images/Golden-circle-what.png" img-alt="Golden circle what" %}}
And what happened then is we are focusing on **WHATs**. Like what we are currently doing, what the company are currently selling. It is understandable because WHATs are the easiest things to see. They are tangible, like your job title, your role in a group.  
  
However, this short-sighted view of the world in general yields short term solutions instead of long term... for everything in our lives.  
{{% /2cols %}}

 
###### Manipulation vs. Inspiration

> ... if you ask most businesses why their customers are their customers, most will tell you it's because of superior quality, features, price or service. In other words, most companies have no clue why their customers are their customers

If they don't know why their customers are their customers, how can they attract new customers ? Sadly, most companies' answer to this is to increase marketing budget without a clue what would the extra money be used for.     

There are two ways to influence human behavior, the book mentions, manipulate it or inspire it. Price, Promotions or Fear are common short term manipulative tactics, which yield immediate results but command no long term loyalty.  

###### Novelty (a.k.a. Innovation)  
Motorola's RAZR phones were pretty amazing. Remembering these ?   

{{% 2cols left-cls="col-sm-5 push-sm-7" right-cls="col-sm-7 pull-sm-5" img-src="images/motorola.jpeg" img-alt="Motorola" %}}{{% blockquote %}}
The combination of metals, such as aircraft-grade aluminum, with new advances, such as an internal antenna and a chemically-etched keypad, led to the formation of a device that measures just 13.9 mm thin.
{{% /blockquote %}}
These are state-of-the-art devices, and at the same time are so innovative that it guaranteed Motorola's future financial success, ... right ?
{{% /2cols %}}

Apparently **NOT**, as it turned out, only less than four years later Motorola's then0-CEO, **Edward Zander** has to stepped down. Its stock traded at 50% of its average value since the launch of RAZR. Motorola made a catastrophic mistake. It confused **Novelty** and **Innovation**.       

##### The Golden circle
{{< img "images/Golden-circle.png" "Golden circle" "width: 300px" >}}  
From outside of the circle moving inward:  

- **WHAT**: What companies do or sell. The products, services or the tangible outputs of a person or a company's effort  
- **HOW**: How companies do **WHAT** they do. **HOWs** are often used to explain how something (the company provides) is different or better  
- **WHY**: WHY do the company exist ? WHY did it do the things that it is doing ? What mission is it trying to achieve ? 

Most companies or people will stop at the first 2 layers and think those define their companies or their lives. But it is just wrong.  

###### It's biology
> Gut decisions don't happen in your stomach  

Gut decisions are usually made when it feels right rather than when it is rational. We don't know where is this "feeling" from. As it turned out, this is how our brain works.

{{< img "images/Golden-circle2.png" "Golden circle" "width: 300px" >}}  
 
- The **neocortex** is the newest area of the brain. It is responsible for **rational and analytical thought and language**
- The middle 2 sections comprise the **limbic brain**. It is responsible for our feelings like trust and loyalty. And get this. **It has no capacity for language**

This is the reason why deep down, you might feel right but you can't really explain the decision. A lot of the time when choosing between products you already know what you are gonna get but spending a lot of time doing research just so that you can rationalize that decision. 

**WHY** touches you in the deepest level of your emotion. Without it, **HOW** and **WHAT** have no direction. This is true at both personal level and organizational level. In fact, the book explained that most of a company's customers buy the product because their WHYs align. And they don't even buy the product for the company's sake. It is for them, to express who they are.      

#### The Split
Starting with WHY gives us the clarity of both **HOW** and **WHAT** should be done not only now but also in the future. A lot of startups started with very clear sense of WHY because the owner/founder was personally managing when its size is small. However, most successful companies experience the **Split** when it grows bigger. This is when their **WHATs** are now totally different from their **WHYs** 

{{< img "images/split.png" "Split happens" "width: 300px" >}}  

#### The Dream
Imagine this, if you wake up every day, excited to start work even though there are going to be difficulty. That is a very good signal, you have it, your WHY. The book further explains various brilliant concepts to find and bring clarity to the WHY

- Law of diffusion of Innovations (from ***The Tipping Point*** by ***Malcolm Gladwell***).  
- Communication is Not about Speaking, It's about listening
- Case studies on companies when WHY got fuzzy

#### Summary
We all need a reason to be alive. We need a reason to wake up every morning and say "*let's create the future*".  

Truth is in this modern world, it's getting harder and harder to experience that. Everyone is getting more and more busy doing everything for the sake of it. Without a clear motive behind, without that gut feeling, days, months or even years passed by meaninglessly in a blink of an eye.  

That **WHY**, is the reason that we are focused, excited and present at every single moment. 

In the book "Man's Search for Meaning", the author, Viktor Frankl, who was a Nazi's concentration camp survivor highlighted the importance of WHY as such:
{{< img "images/meaning.jpg" "Meaning" >}}  

<h3 class="text-center">Find your WHY !!!</h3>
