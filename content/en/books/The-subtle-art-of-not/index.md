---
title: The Subtle Art of Not Giving a F*ck
author: Mark Manson
rating: 4
affiliate: amazon
afflink: https://amzn.to/44vL96m
tags: ["english"]
---



#### Introduction:
If there are two most important things I've got from this book, those will be:  
  
 - **Choose to be responsible for things that happened in your life** regardless of whether you are the cause of it.  
 - **Action isn't just the effect of motivation, it's also the cause of it**. So not only motivation -> action but also action -> motivation

From my personal experience, these two key ideas switch your brain from going on defense to offense.  
It stops you from waiting for the **right moment**, **right condition** to act. Rather, just act, make mistakes and move on.
And that idea alone is already a very powerful concept.

#### Book Summary  

{{% chapter id="section1" cls="wow fadeInRight" title="Chapter 1: Don't try" %}}
##### Welcome to the feedback loop from Hell!
This part of the book explains why we are the only animals that experience this type of pressure and how to resolve it.  

> The desire for more positive experience is itself a negative experience. And, paradoxically, the acceptance of one's 
negative experience is itself a positive experience

Say what ? Read it again...   

##### The Subtle art of not giving a Fuck  
{{< img "images/mess.jpg" "Feedback loop" >}}  

This explains the mission of the book.  
The book is not trying to tell you NOT to give ANY fuck about anything (like your first impression on the title)  
It wants you, however, to consciously CHOOSE the fucks you wanna give. And why is it important ?

> Because when you give too many fucks - when you give a fuck about everyone and everything - you will feel that you're perpetually entitled to be comfortable and 
happy at all times, that everything is supposed to be just exactly the fucking way *you want it to be*. This is a sickness. And it will eat you alive. 
You will see every day adversity as an injustice, every inconvenience as a personal slight, every disagreement as a betrayal

{{% /chapter %}}

{{% chapter id="section2" cls="wow fadeInLeft" title="Chapter 2: Happiness IS a Problem" %}}
##### Happiness Comes from Solving Problems  
While the majority of us were being taught (or expected) to pursue happiness, not a lot understands where it comes from, especially for ourselves.  

> Happiness is a constant work-in-progress, because solving problems is a constant work-in-progress -- 
the solution to today's problems will lay the foundation for tomorrow's problems, and so on.
True happiness occurs only when you find the problems you enjoy having and enjoy solving

{{< img "images/problem.jpg" "Problems" >}}  

However, most people think their life is not that simple. And it is because of 2 main reasons:
  
- **Denial**: "My life does not have a(that) problem"
- **Victim Mentality**: "There is nothing I can do about this" even if they actually could.  

With this two excuses, what they do instead is having more fucks to give about everything, trying to forget the real problems that need solving.
{{% /chapter %}}

{{% chapter id="section3" cls="wow fadeInRight" title="Chapter 3: You are not special" %}}
**High self-esteem**, or having positive thoughts and feelings about oneself have been emphasized in the U.S. from 1970s. 
The implementation includes grade inflation, participation awards and bogus trophies were introduced to make kids feel better about their 
lack of achievements. They are also encouraged to think of in what ways they are special.  

This, however, leads the whole generation to some delusional and exceptional image of themselves.
Anything that said otherwise will be discarded. The confidence is bulletproof.  

That sense (or need) of being special always, feeling good about oneself always, or ENTITLEMENT, is itself a sickness.

##### The Tyranny of Exceptionalism  
Most of us won't be exceptional in all aspect of our lives. It is quite NORMAL.
However, this is no longer acceptable in the current society.  

> The problem is that the pervasiveness of technology and mass marketing is screwing up a lot of people's expectation for themselves.
> The inundation of the exceptional makes people feel worse about themselves, makes them feel that they need to be more extreme, more radical,
> and more self-assured to get noticed or even matter.

##### Then what's the point if I'm not going to be special ?   
*Being special* is a product that's been sold to you by society, by companies that need you to buy their product to become better, smarter, sexier.

> It is basically just jerking off your ego. It's a message that tastes good going down, but in reality is nothing more than empty calories that make you 
> emotionally fat and bloated, the proverbial Big Mac for your heart and your brain

The power is coming from doing tough works, from the sufferings (like eating veggies - using previous proverb), not from magically feeling good.
{{% /chapter %}}


{{% chapter id="section4" cls="wow fadeInLeft" title="Chapter 4: The Value of Suffering" %}}
If suffering is inevitable, the question we should answer is not "How to stop suffering ?", but "Why am I suffering, and for what purpose ?"  
Most of the time we suffer because our values are violated, or are unmet. This chapter dedicates to guide how to pick good and bad values.

##### Shitty Values  
- Pleasure  
- Always Being Right    
- Material Success  
- Staying Positive

##### Good/Bad Values    

{{< table "table table-striped table-bordered" >}}
|Good values are:                 |Bad values are:
|---------------------------------|----------------------------------
|  - Reality-based                |  - Superstitious
|  - Socially constructive        |  - Socially destructive 
|  - Immediate and controllable   |  - Not immediate or controllable
{{< /table >}}

For example:  
Honesty is a good value because you have complete control over it, and it benefits others.  
Popularity is a bad value because you have no control over it, and it based mostly on feelings.
{{% /chapter %}}


{{% chapter id="section5" cls="wow fadeInRight" title="Chapter 5: You Are Always Choosing" %}}
In life, we don't always control what happens to us, but we are always the one who interprets
what they mean. And that could be the difference between feeling excited or terrified about the same event.
Because we always "choosing the metrics by which we measure everything" whether we want or not.

> It comes back to how, in reality, there is no such thing as not giving a single fuck. It's impossible.
> ... To not give a fuck about *anything* is still to give a fuck about something.  

{{< img "images/driving.jpg" "Responsibility" >}}  

##### The Responsibility/Fault Fallacy  

> "With great responsibility comes great power"
> The more we choose to accept responsibility in our lives, the more power we will exercise over our lives

Our culture often equates **have responsibility** and **be at fault**. Therefore, we often denied the **responsibility**
of our problems because we thought it means they are our faults. That is not true: there are problems that we aren't at fault for,
yet we are still responsible for.  

For example, someone dropped a new born baby in front of your doorstep one fine morning. That is not something you are at fault for.
But you still need to decide what you should do with the baby. There goes your responsibility.  

##### Victimhood Chic  
The fallacy above allows people to pass off responsibility for solving their problems to others.
Blaming others is the simplest way to alleviate the responsibility burden and get people a temporary high of moral righteousness.  

> "Victimhood Chic" is in style on both the right and the left, among both the rich and the poor.
> In fact, this may be the first time in human history that every single demographic group has felt
> unfairly victimized simultaneously

There is then so many fucks to give and so little time. The truth is those aren't the real, right problems to solve.
If you take responsibility for your life, you would know what fucks you should give and what you should not.
{{% /chapter %}}

{{% chapter id="section6" cls="wow fadeInLeft" title="Chapter 6, 7: You're wrong About Everything ... And Failure is the way forward" %}}
We are wrong every single time we make an assumption and that's okay because that's the nature of growth.  

> Growth is an endlessly iterative process. When we learn something new, we don't go from "wrong" to "right".
> We go from wrong to slightly less wrong ...
> Being wrong opens us up to the possibility of change. Being wrong bring the opportunity of growth.

So don't hold your opinions too strongly, have less certain of Yourself. Question yourself when you are absolutely certain:  
1. What if I'm wrong ?
2. What would it mean if I were wrong ?
3. Would being wrong create a better or worse problem for me and for others ?
{{< img "images/Question.jpg" "Question" >}}

> Pain is part of the process (*to achieve anything*). It's important to feel it.
> Because if you just chase after highs to cover up the pain, if you continue to indulge in entitlement
> and delusional positive thinking, if you continue to overindulge in various substances or activities 
> then you'll never generate the requisite motivation to actual change

So you need to accept pain (or struggle) as part of the process to grow. Another important concept is called 
the **Do Something** principle. It simply means that when you want to achieve something, just take action first,
don't wait for the motivation. That is quite the opposite of most self-help book will teach you.  

The **Do Something** principle reverts that process and it does make sense. In reality, what happens is like an endless loop:  

> Inspiration -> Motivation -> Action -> Inspiration -> Motivation

As we take action, gain some small wins, we will be more inspired to continue. And the effect will snowball into more visible results,
which in turn, create even bigger inspiration.
{{% /chapter %}}

The last two chapters of the book, the author explains more why filtering out the fucks we should give can change our lives.
Also, what is the drives behind that makes us give too many fucks unnecessarily.

<hr/>
